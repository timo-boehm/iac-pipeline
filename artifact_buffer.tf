### Credentials for local pipeline to read and write to defined S3 bucket

# Technical user and access keys
resource "aws_iam_user" "local_pipeline" {
  name = "local_pipeline"
  path = "/technical_user/"
}

resource "aws_iam_access_key" "local_pipeline" {
  # NOTE: this is added for convenience, do not use in production!
  user = aws_iam_user.local_pipeline.name
}

# Group for users that want to make IaC suggestions
resource "aws_iam_group" "iac_suggestions" {
  name = "read_resources_and_suggest_changes"
  path = "/technical_gropus/"
}
resource "aws_iam_group_membership" "local_pipelines" {
  name = "local_pipelines"
  users = [
    aws_iam_user.local_pipeline.name
  ]
  group = aws_iam_group.iac_suggestions.name
}

# Policies for the group
data "aws_iam_policy" "read_resources" {
  arn = "arn:aws:iam::aws:policy/ReadOnlyAccess"
}

resource "aws_iam_group_policy_attachment" "read_policy_attachment" {
  group      = aws_iam_group.iac_suggestions.name
  policy_arn = data.aws_iam_policy.read_resources.arn
}

data "aws_iam_policy_document" "write_plan_to_bucket" {
  statement {
    sid = "AllowPutObject"
    actions = [
      "s3:PutObject"
    ]
    resources = [
      "arn:aws:s3:::${aws_s3_bucket.plan_exchange.arn}"
    ]
  }
}

resource "aws_iam_policy" "put_object_to_bucket" {
  name        = "put_object_policy"
  path        = "/"
  description = "used by local pipelines to upload terraform plans into exchange bucket"
  policy      = data.aws_iam_policy_document.write_plan_to_bucket.json
}

resource "aws_iam_group_policy_attachment" "test-attach" {
  group      = aws_iam_group.iac_suggestions.name
  policy_arn = aws_iam_policy.put_object_to_bucket.arn
}

### S3 bucket to upload terraform plan files to
resource "random_string" "plan_bucket_suffix" {
  length  = 16
  lower   = true
  upper   = false
  number  = false
  special = false
}

resource "aws_s3_bucket" "plan_exchange" {
  bucket        = "exchange-bucket-${random_string.plan_bucket_suffix.result}"
  acl           = "private"
  force_destroy = true
  versioning {
    enabled = true
  }
}