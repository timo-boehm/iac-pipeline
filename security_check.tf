# Security Checks Project

resource "aws_codebuild_project" "security_check" {
  name         = "security_check"
  description  = "runs static security checks on terraform plan"
  service_role = aws_iam_role.security_check.arn
  source {
    type      = "CODEPIPELINE"
    buildspec = file("./buildspecs/security_check.yml")
  }
  artifacts {
    type = "CODEPIPELINE"
  }
  environment {
    type         = "LINUX_CONTAINER"
    compute_type = "BUILD_GENERAL1_SMALL"
    image        = "bridgecrew/checkov:latest"
  }

}

resource "aws_iam_role" "security_check" {
  name = "security_check_terraform_plan"
  path = "/technical_roles/"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "codebuild.amazonaws.com"
        }
      },
    ]
  })
}

data "aws_iam_policy_document" "security_checks" {
  statement {
    sid = "AllowAccessToCloudWatch"
    actions = [
      "logs:*"
    ]
    resources = ["*"]
  }
  statement {
    sid = "AllowAccessToS3"
    actions = [
      "s3:*"
    ]
    resources = [
      aws_s3_bucket.artifact_store.arn,
      "${aws_s3_bucket.artifact_store.arn}/*"
    ]
  }
}

resource "aws_iam_role_policy" "security_checks" {
  name   = "access_rights"
  role   = aws_iam_role.security_check.id
  policy = data.aws_iam_policy_document.security_checks.json
}