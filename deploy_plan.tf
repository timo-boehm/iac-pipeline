# Terraform Deployment Project
resource "aws_codebuild_project" "iac_deployment" {
  name         = "iac_deployment"
  description  = "applies terraform plan"
  service_role = aws_iam_role.iac_deployment.arn
  source {
    type      = "CODEPIPELINE"
    buildspec = file("./buildspecs/deploy_plan.yml")
  }
  artifacts {
    type = "CODEPIPELINE"
  }
  environment {
    type         = "LINUX_CONTAINER"
    compute_type = "BUILD_GENERAL1_SMALL"
    image        = "hashicorp/terraform:1.0.5" # Fixed Version
  }
}

resource "aws_iam_role" "iac_deployment" {
  name = "deployment_terraform_plan"
  path = "/technical_roles/"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "codebuild.amazonaws.com"
        }
      },
    ]
  })
}

data "aws_iam_policy_document" "iac_deployment" {
  statement {
    sid = "AllowAccessToCloudWatch"
    actions = [
      "logs:*"
    ]
    resources = ["*"]
  }
  statement {
    sid = "AllowAccessToS3"
    actions = [
      "s3:*"
    ]
    resources = [
      aws_s3_bucket.artifact_store.arn,
      "${aws_s3_bucket.artifact_store.arn}/*"
    ]
  }
}

resource "aws_iam_role_policy" "iac_deployment" {
  name   = "access_rights"
  role   = aws_iam_role.iac_deployment.id
  policy = data.aws_iam_policy_document.iac_deployment.json
}