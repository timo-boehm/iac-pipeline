# CodePipeline --> https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/codepipeline

resource "aws_codepipeline" "cloud_pipeline" {
  name     = "iac_deployment_piepline"
  role_arn = aws_iam_role.cloud_pipeline.arn
  artifact_store {
    location = aws_s3_bucket.artifact_store.bucket
    type     = "S3"
  }
  stage {
    name = "Source"
    action {
      category         = "Source"
      owner            = "AWS"
      name             = "SourcePlan"
      provider         = "S3"
      version          = "1"
      output_artifacts = ["terraform_plan"]
      configuration = {
        S3Bucket             = aws_s3_bucket.plan_exchange.bucket
        S3ObjectKey          = "plan.zip"
        PollForSourceChanges = true
      }
    }
  }
  stage {
    name = "Checks"
    action {
      category        = "Test"
      owner           = "AWS"
      name            = "SecurityChecks"
      provider        = "CodeBuild"
      version         = "1"
      input_artifacts = ["terraform_plan"]
      configuration = {
        ProjectName = aws_codebuild_project.security_check.name
      }
    }
  }
  stage {
    name = "Approval"
    action {
      category = "Approval"
      owner    = "AWS"
      name     = "ManualApproval"
      provider = "Manual"
      version  = "1"
      configuration = {
        CustomData         = "check tf.plan in plan.zip for review"
        ExternalEntityLink = aws_s3_bucket.plan_exchange.id
        NotificationArn    = aws_sns_topic.pipeline_notifications.arn
      }
    }
  }
  stage {
    name = "Deployment"
    action {
      category        = "Build" # "Deploy" is reserved for other AWS services (https://docs.aws.amazon.com/codepipeline/latest/userguide/reference-pipeline-structure.html)
      owner           = "AWS"
      name            = "TerraformDeployment"
      provider        = "CodeBuild"
      version         = "1"
      input_artifacts = ["terraform_plan"]
      configuration = {
        ProjectName = aws_codebuild_project.iac_deployment.name
      }
    }
  }
}

resource "aws_iam_role" "cloud_pipeline" {
  name = "check_and_deploy_iac"
  path = "/technical_roles/"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "codepipeline.amazonaws.com"
        }
      },
    ]
  })
}

data "aws_iam_policy_document" "codepipeline_policy" {
  statement {
    sid = "AllowCodePipelineToAccessS3"
    actions = [
      "s3:*"
    ]
    resources = [
      aws_s3_bucket.plan_exchange.arn,
      "${aws_s3_bucket.plan_exchange.arn}/*",
      aws_s3_bucket.artifact_store.arn,
      "${aws_s3_bucket.artifact_store.arn}/*"
    ]
  }
  statement {
    sid = "AllowCodePipelineToWorkWithCodeBuild"
    actions = [
      "codebuild:*"
    ]
    resources = [
      aws_codebuild_project.security_check.arn,
      aws_codebuild_project.iac_deployment.arn
    ]
  }
  statement {
    sid = "AllowsCodePipelineToPublishToSNS"
    actions = [
      "sns:Publish"
    ]
    resources = [aws_sns_topic.pipeline_notifications.arn]
  }
}

resource "aws_iam_role_policy" "codepipeline_policy" {
  name   = "access_s3_buckets"
  role   = aws_iam_role.cloud_pipeline.id
  policy = data.aws_iam_policy_document.codepipeline_policy.json
}

resource "random_string" "pipeline_bucket_suffix" {
  length  = 16
  lower   = true
  upper   = false
  number  = false
  special = false
}

resource "aws_s3_bucket" "artifact_store" {
  bucket        = "artifact-bucket-${random_string.pipeline_bucket_suffix.result}"
  acl           = "private"
  force_destroy = true
  versioning {
    enabled = true
  }
}

### Notification Topic
resource "aws_sns_topic" "pipeline_notifications" {
  name = "notifications_of_cloud_pipeline"
}