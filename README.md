# Infrastructure-as-Code Pipeline

AWS part of a loosely coupled IaC pipeline concept. This repo relates to an article to be published in Softwerker #19.

## Setup
```terraform apply``` is enough to deploy all necessary resources (assuming that you configured your AWS credentials correctly). To create a valid input file, follow these three steps:

* ```terraform plan -out tf.plan``` to create a plan.
* ```terraform show -json tf.plan | jq '.' > tf.json``` to provide a format that ```checkov``` can use for security checks.
* ```zip plan.zip tf.plan tf.json``` to combine them in the expected ```zip``` file.

After that, upload the file into the _S3_ bucket and _CodePipeline_ should start as expected.

## Notes
_This repo is meant for illustration, not as the basis for production-ready code!_

Some remarks about specific parts of the ```terraform``` code:

* The pipeline is hard-coded to expect a ```plan.zip``` file. In production, you would use a parameter instead.
* The IAM-policies are largely restricted by resources only. Remember to follow _least-privilege_ ideas on all levels in a real-world scenario.
* The IAM policy for the _CodeBuild_ project that covers deployment is not enough to actually deploy a plan. That's to avoid accidental deployments.
* Although there is an _SNS_ topic, you'd need to add subscriptions as described in the article. You'd also need to implement _CodeStar_ notifications for information about pipeline states and events.
* The ```terraform``` version is fixed due to the Docker image used by the _CodeBuild_ project. You'd need to adjust it to the version you prefer.
* Security checks done by ```checkov``` are limited to a single check so that most plans should pass the check.